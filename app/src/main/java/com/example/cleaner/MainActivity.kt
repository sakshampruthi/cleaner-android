package com.example.cleaner

import android.animation.Animator
import android.bluetooth.BluetoothAdapter
import android.content.ComponentName
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import java.io.BufferedReader
import java.io.InputStreamReader


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        cleanImage.visibility = View.GONE
        temperature.text = "${temp()}°C"

        autoStart.setOnClickListener {
            for (intent in AUTO_START_INTENTS){
                if (packageManager.resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY) != null) {
                    startActivity(intent)
                    break
                }
            }
        }

        optimizeMode.setOnClickListener {
            brightness(0.3F)
            toast("Battery life increased")
            animate()
            bluetooth()
        }
        ramMode.setOnClickListener {
            animate()
            kill()
            freeMemory()
            toast("Ram freed")
        }
        tempMode.setOnClickListener {
            brightness(0.3F)
            animate()
            kill()
            bluetooth()
            freeMemory()
            toast("Temperature Optimized")
            temperature.text = "${temp()}°C"
        }
    }

    private fun brightness(bright: Float){
        val layout = window.attributes
        layout.screenBrightness = bright
        window.attributes = layout
    }

    private fun kill(){
        val killBackground = KillBackground(this)
        killBackground.execute()
    }
    private fun freeMemory() {
        System.runFinalization()
        Runtime.getRuntime().gc()
        System.gc()
    }

    private fun animate(){
        cleanLottie.visibility = View.VISIBLE
        cleanLottie.speed =1.0F
        cleanLottie.playAnimation()

        cleanLottie.addAnimatorListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator) {}
            override fun onAnimationEnd(animation: Animator) {
                cleanLottie.cancelAnimation()
            }
            override fun onAnimationCancel(animation: Animator) {}
            override fun onAnimationRepeat(animation: Animator) {
            }
        })

    }
    private fun temp():Float{
        val process: Process
        return try {
            process =
                Runtime.getRuntime().exec("cat sys/class/thermal/thermal_zone0/temp")
            process.waitFor()
            val reader = BufferedReader(InputStreamReader(process.inputStream))
            val line: String = reader.readLine()
            if (line != null) {
                val temp = line.toFloat()
                return temp / 1000.0f
            } else {
                51.0f
            }
        } catch (e: Exception) {
            e.printStackTrace()
            0.0f
        }
    }
     private fun bluetooth(){
         val adapter = BluetoothAdapter.getDefaultAdapter()
         if (adapter != null) {
             if (adapter.state == BluetoothAdapter.STATE_ON)
                 adapter.disable()
         }
     }
   companion object {
       private val AUTO_START_INTENTS = arrayOf(
           Intent().setComponent(
               ComponentName(
                   "com.samsung.android.lool",
                   "com.samsung.android.sm.ui.battery.BatteryActivity"
               )
           ),
           Intent("miui.intent.action.OP_AUTO_START").addCategory(Intent.CATEGORY_DEFAULT),
           Intent().setComponent(
               ComponentName(
                   "com.miui.securitycenter",
                   "com.miui.permcenter.autostart.AutoStartManagementActivity"
               )
           ),
           Intent().setComponent(
               ComponentName(
                   "com.letv.android.letvsafe",
                   "com.letv.android.letvsafe.AutobootManageActivity"
               )
           ),
           Intent().setComponent(
               ComponentName(
                   "com.huawei.systemmanager",
                   "com.huawei.systemmanager.optimize.process.ProtectActivity"
               )
           ),
           Intent().setComponent(
               ComponentName(
                   "com.coloros.safecenter",
                   "com.coloros.safecenter.permission.startup.StartupAppListActivity"
               )
           ),
           Intent().setComponent(
               ComponentName(
                   "com.coloros.safecenter",
                   "com.coloros.safecenter.startupapp.StartupAppListActivity"
               )
           ),
           Intent().setComponent(
               ComponentName(
                   "com.oppo.safe",
                   "com.oppo.safe.permission.startup.StartupAppListActivity"
               )
           ),
           Intent().setComponent(
               ComponentName(
                   "com.iqoo.secure",
                   "com.iqoo.secure.ui.phoneoptimize.AddWhiteListActivity"
               )
           ),
           Intent().setComponent(
               ComponentName(
                   "com.iqoo.secure",
                   "com.iqoo.secure.ui.phoneoptimize.BgStartUpManager"
               )
           ),
           Intent().setComponent(
               ComponentName(
                   "com.vivo.permissionmanager",
                   "com.vivo.permissionmanager.activity.BgStartUpManagerActivity"
               )
           ),
           Intent().setComponent(
               ComponentName(
                   "com.asus.mobilemanager",
                   "com.asus.mobilemanager.entry.FunctionActivity"
               )
           ).setData(
               Uri.parse("mobilemanager://function/entry/AutoStart")
           )
       )
   }
}